const asyncForEach = async (items, callback) => {
  for (let index = 0; index < items.length; index++) {
    await callback(items[index], index, items)
  }
}

export default asyncForEach
