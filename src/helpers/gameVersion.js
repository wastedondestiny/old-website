const GameVersion = Object.freeze({
  DESTINY: 1,
  DESTINY2: 2,
  UNKNOWN: 0
})

export default GameVersion
